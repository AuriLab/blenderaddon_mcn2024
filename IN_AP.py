# Master Création Numérique 2023-2024
# Add-on Blender
# Aurian Payan
# Issa Ngom

import bpy
import math
import bgl
import blf
import mathutils
import bpy_extras

# Opérateur pour mesurer la distance entre deux objets
class MeasureOperator(bpy.types.Operator):
    bl_idname = "view3d.measure_operator"
    bl_label = "Mesure de Distance"
    bl_description = "Mesure la distance entre deux objects"

    @classmethod
    def poll(cls, context):
        return context.space_data.type == 'VIEW_3D'

    def modal(self, context, event):
        if event.type == 'LEFTMOUSE' and event.value == 'PRESS':
            region = context.region
            rv3d = context.region_data
            coord = mathutils.Vector((event.mouse_region_x, event.mouse_region_y))
            view_vector = bpy_extras.view3d_utils.region_2d_to_vector_3d(region, rv3d, coord)
            
            cursor3d = bpy.context.scene.cursor.location
            
            selected_objects = [obj for obj in bpy.context.selected_objects if obj.type == 'MESH']
            
            if len(selected_objects) == 2:
                distance = (selected_objects[0].location - selected_objects[1].location).length
                self.report({'INFO'}, "Distance between the two objects: {:.2f}".format(distance))
                return {'FINISHED'}
                
        return {'PASS_THROUGH'}

    def invoke(self, context, event):
        args = (self, context)
        self._handle = bpy.types.SpaceView3D.draw_handler_add(draw_callback_px, args, 'WINDOW', 'POST_PIXEL')
        context.window_manager.modal_handler_add(self)
        return {'RUNNING_MODAL'}

# Définition de la fonction de dessin pour afficher les instructions
def draw_callback_px(self, context):
    font_id = 0
    blf.size(font_id, 20, 72)
    blf.position(font_id, 15, 15, 0)
    blf.draw(font_id, "Cliquez sur deux objets pour mesurer leur distance")

# Opérateur pour ajouter une subdivision aux objets sélectionnés
class Generate_subdiv_selected_objects(bpy.types.Operator):
    "Subdivision de la sélection"
    
    bl_idname = "mesh.add_subdiv_selected"
    bl_label = "Subdivision Surface"
    bl_options = {"REGISTER", "UNDO"}
    
    levels: bpy.props.IntProperty(
        name="Levels Viewport",
        default=2,
        min=0,
        max=6,
        description="Number of subdivision"
    )

    render_levels: bpy.props.IntProperty(
        name="Render",
        default=2,
        min=0,
        max=6,
        description="Number of subdivision when rendering"
    )
    
    shade_smooth: bpy.props.BoolProperty(
        name="Shade Smooth",
        default=True,
        description="Apply Smooth Shading to the mesh",
    )

    def execute(self, context):
        objs = bpy.context.selected_objects
        for obj in objs:
            bpy.ops.object.subdivision_set(level=self.levels, relative=False)
            
            # Trouver le modificateur Subdivision
            subdivision_modifier = None
            for modifier in obj.modifiers:
                if modifier.type == 'SUBSURF':
                    subdivision_modifier = modifier
                    break
            
            # Si un modificateur Subdivision est trouvé, définir render_levels
            if subdivision_modifier:
                subdivision_modifier.render_levels = self.render_levels
                
            if self.shade_smooth:
                bpy.ops.object.shade_smooth()
            else:
                bpy.ops.object.shade_flat()
        return {"FINISHED"}

# L'opérateur pour ajouter un bevel aux objets sélectionnés    
class Generate_bevel_selected_objects(bpy.types.Operator):
    "Bevel de la sélection"
    
    bl_idname = "mesh.add_bevel_selected"
    bl_label = "Bevel"
    bl_options = {"REGISTER", "UNDO"}
    
    bevel_target: bpy.props.EnumProperty(
        name="Bevel Target",
        items=[
            ('EDGES', "Edges", "Bevel Edges"),
            ('VERTICES', "Vertices", "Bevel Vertices"),
        ],
        default='EDGES',
        description="Affect edges or vertices"
    )
    
    offset_type: bpy.props.EnumProperty(
        name="Width Type",
        items=[
            ('OFFSET', "Offset", "Offset"),
            ('WIDTH', "Width", "Width"),
            ('DEPTH', "Depth", "Depth"),
            ('ABSOLUTE', "Absolute", "Absolute"),
        ],
        default='OFFSET',
        description="What distance Width measures"
    )
    
    width: bpy.props.FloatProperty(
        name="Amount",
        default=0.1,
        min=0,
        max=100,
        unit="LENGTH",
        description="Bevel width"
    )

    segments: bpy.props.IntProperty(
        name="Segments",
        default=1,
        min=1,
        max=100,
        description="Number of segments for round edges/verts"
    )
    
    limit_method: bpy.props.EnumProperty(
        name="Limit Method",
        items=[
            ('NONE', "None", "No Limit"),
            ('ANGLE', "Angle", "Limit by Angle"),
        ],
        default='ANGLE',
        description="Method for limiting the bevel effect"
    )
    
    angle_limit: bpy.props.FloatProperty(
        name="Angle",
        default=30 * math.pi / 180, # Angle en radians
        min=0,
        max=180,
        unit="ROTATION",
        description="Maximum bevel angle"
    )

    def execute(self, context):
        objs = bpy.context.selected_objects
        for obj in objs:
            bpy.context.view_layer.objects.active = obj  # Définir l'objet actif
            
            # Ajouter le modificateur Bevel
            bevel_modifier = obj.modifiers.new(name="Bevel", type='BEVEL')
            bevel_modifier.offset_type = self.offset_type
            bevel_modifier.width = self.width
            bevel_modifier.segments = self.segments
            bevel_modifier.limit_method = self.limit_method
            bevel_modifier.angle_limit = self.angle_limit
            
            # Déterminer quelles parties de l'objet est à biseauter
            if self.bevel_target == 'EDGES':
                bevel_modifier.affect = 'EDGES'
            else:
                bevel_modifier.affect = 'VERTICES'
            
        return {"FINISHED"}

# L'opérateur pour apply les modificateurs des objets sélectionnés
class Apply_modifier_selected_objects(bpy.types.Operator):
    "Apply Modifiers to Selected Objects"
    
    bl_idname = "mesh.apply_modifiers_selected"
    bl_label = "Apply Modifiers"
    bl_options = {"REGISTER", "UNDO"}

    def execute(self, context):
        for obj in bpy.context.selected_objects:
            bpy.context.view_layer.objects.active = obj
            bpy.ops.object.convert(target='MESH', keep_original=False)
        return {"FINISHED"}

# L'opérateur pour supprimer les modificateurs des objets sélectionnés
class Remove_modifier_selected_objects(bpy.types.Operator):
    "Supprimer modifier de la sélection"
    
    bl_idname = "mesh.remove_modifier_selected"
    bl_label = "Remove Modifier"
    bl_options = {"REGISTER", "UNDO"}
    
    shade_flat: bpy.props.BoolProperty(
        name="Shade Flat",
        default=True,
        description="Apply Flat Shading to the mesh",
    )
    
    def execute(self, context):
        objs = bpy.context.selected_objects
        for obj in objs:
            obj.modifiers.clear()
            if self.shade_flat==True:
                bpy.ops.object.shade_flat()
            else:
                bpy.ops.object.shade_smooth()
        return {"FINISHED"}

# Le panneau personnalisé contenant tous les boutons de l'addon

class MCN_PT_IN_AP(bpy.types.Panel):
    """"""
    bl_label = "IN_AP"
    bl_idname = "MCN_PT_IN_AP"
    bl_space_type = 'VIEW_3D'
    bl_region_type = 'UI'
    bl_category = "Tool"
    bl_parent_id = "MCN_PT_Main_Panel"
    bl_options = {'DEFAULT_CLOSED'}

    def draw(self, context):
        layout = self.layout
        
        row = self.layout.row() # Ajouter une nouvelle ligne au pannel
        row.operator("mesh.add_subdiv_selected", icon="MOD_SUBSURF") # Ajouter un bouton
        
        row = self.layout.row()
        row.operator("mesh.add_bevel_selected", icon="MOD_BEVEL")
        
        self.layout.separator() # Ajouter un espace
        
        row = self.layout.row()
        row.operator("mesh.apply_modifiers_selected", icon="MODIFIER_ON", text="Apply Modifiers")
        
        row = self.layout.row()
        row.operator("mesh.remove_modifier_selected", icon="PANEL_CLOSE")
        
        self.layout.separator() # Ajouter un espace
        
        row = layout.row()
        row.operator("view3d.measure_operator", icon="MOD_EXPLODE")


# Enregistrement des classes
classes = (
    MeasureOperator,
    Generate_subdiv_selected_objects,
    Generate_bevel_selected_objects,
    Apply_modifier_selected_objects,
    Remove_modifier_selected_objects,
    MCN_PT_IN_AP
)

def register():
    for cls in classes:
        bpy.utils.register_class(cls)

def unregister():
    for cls in classes:
        bpy.utils.unregister_class(cls)


#if __name__ == "__main__": # à supprimer à la fin
    #register()